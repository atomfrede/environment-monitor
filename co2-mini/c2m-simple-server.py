#!/usr/bin/env python2

import sys, fcntl, time, datetime, re, threading

import SimpleHTTPServer
import SocketServer


class Co2Monitor:
    ctmp = list(((c >> 4) | (c << 4)) & 0xff \
                for c in [0x48,  0x74,  0x65,  0x6D,  0x70,  0x39,  0x39,  0x65])
    shuffle = [2, 4, 0, 7, 1, 6, 5, 3]
    key = [0xc4, 0xc6, 0xc0, 0x92, 0x40, 0x23, 0xdc, 0x96]

    def __init__(self):
        self.ops = {}
        self.temp = -1000
        self.co2 = -1
        self.time = 0
        self.lock = threading.Lock()

    def decrypt(self, data):
        phase1 = list(data[i] for i in self.shuffle)
        phase2 = list(phase1[i] ^ self.key[i] for i in range(8))
        phase3 = list(((phase2[i] >> 3) | (phase2[(i - 1 + 8) % 8] << 5)) & 0xff for i in range(8))
        out = list((0x100 + phase3[i] - self.ctmp[i]) & 0xff for i in range(8))
        return out

    def fileno(self):
        return self.hidfp

    def get_status(self):
        self.lock.acquire(True)
        result = (self.time, self.temp, self.co2)
        self.lock.release()
        return result

    def set_temp(self, val):
        self.lock.acquire(True)
        self.temp = val
        self.time = datetime.datetime.now()
        self.lock.release()

    def set_co2(self, val):
        self.lock.acquire(True)
        self.co2 = val
        self.time = datetime.datetime.now()
        self.lock.release()

    def open(self, hidraw):
        self.hidfp = open(hidraw, "a+b", 0)

        HIDIOCSFEATURE_9 = 0xC0094806
        set_report = "\x00" + "".join(chr(e) for e in self.key)
        fcntl.ioctl(self.hidfp, HIDIOCSFEATURE_9, set_report)

    def run(self):
        self.ops = {}

        while True:
            data = list(ord(e) for e in self.hidfp.read(8))
            decrypted = self.decrypt(data)
            if decrypted[4] != 0x0d or (sum(decrypted[:3]) & 0xff) != decrypted[3]:
                print data, " => ", decrypted,  "Checksum error"
            else:
                op = decrypted[0]
                val = decrypted[1] << 8 | decrypted[2]

                if op == 0x42:
                    self.set_temp(val / 16.0 - 273.15)
                elif op == 0x50:
                    self.set_co2(val)

                #self.ops[op] = True
                #if 0x50 in self.ops and 0x42 in self.ops:
                #    print "%s\t%2.2f\t%4i" % (self.time.isoformat(), self.temp, self.co2)
                #    self.ops = {}


class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    @classmethod
    def set_monitor(cls, co2):
        cls.co2 = co2

    log_file = open('/tmp/co2-server.log', 'w')
    def log_message(self, format, *args):
        self.log_file.write("%s - - [%s] %s\n" %
                            (self.client_address[0],
                             self.log_date_time_string(),
                             format%args))
        self.log_file.flush()

    def do_GET(self):
        resp = self.get_co2()
        if len(resp) > 0:
            self.wfile.write(resp)
        else:
            pass #SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

    def do_HEAD(self):
        resp = self.get_co2()
        if len(resp) == 0:
            pass #SimpleHTTPServer.SimpleHTTPRequestHandler.do_HEAD(self)

    def get_co2(self):
        path = self.path.split('?',1)
        args = ""
        if len(path) > 1:
            args = path[1]
        path = path[0]
        path = path.split('#',1)[0]
        args = args.split('#',1)[0]
        if path == "/co2-status.txt":
            (timestamp, temp, co2) = MyRequestHandler.co2.get_status()
            color = "#009900"
            if co2 >= 800 and co2 < 1200:
                color = "#ff9900"
            elif co2 >= 1200:
                color = "#990000"
            text = "%s %2.1f\xc2\xb0\n%4i" % (color, temp, co2)
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Content-Length", str(len(text)))
            self.send_header("Last-Modified", timestamp.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            self.end_headers()
            return text
        return ""

if __name__ == "__main__":
    co2 = Co2Monitor()
    co2.open(sys.argv[1])

    Handler = MyRequestHandler
    Handler.co2 = co2
    SocketServer.ThreadingTCPServer.allow_reuse_address = True
    server = SocketServer.ThreadingTCPServer(('0.0.0.0', 8080), Handler)

    thread = threading.Thread(target=server.serve_forever)
    thread.setName('WebServer')
    thread.setDaemon(True)
    thread.start()

    co2.run()

    sys.exit(0)
