#!/usr/bin/env python2

import sys, fcntl, time, datetime, re, threading
from string import Template
import subprocess

import SimpleHTTPServer
import SocketServer

from co2store import Co2Store


class Co2Monitor:
    ctmp = list(((c >> 4) | (c << 4)) & 0xff \
                for c in [0x48,  0x74,  0x65,  0x6D,  0x70,  0x39,  0x39,  0x65])
    shuffle = [2, 4, 0, 7, 1, 6, 5, 3]
    key = [0xc4, 0xc6, 0xc0, 0x92, 0x40, 0x23, 0xdc, 0x96]

    def __init__(self):
        self.ops = {}
        self.temp = -1000
        self.co2 = -1
        self.time = 0
        self.lock = threading.Lock()
        self.interval = 3    # minutes
        self.ivhelp = -1     # helper for detecting the interval
        # for building an average
        self.numsamples = 0
        self.co2sum = 0
        self.tempsum = 0
        self.co2store = Co2Store("/tmp/co2store.csv")

    def decrypt(self, data):
        phase1 = list(data[i] for i in self.shuffle)
        phase2 = list(phase1[i] ^ self.key[i] for i in range(8))
        phase3 = list(((phase2[i] >> 3) | (phase2[(i - 1 + 8) % 8] << 5)) & 0xff for i in range(8))
        out = list((0x100 + phase3[i] - self.ctmp[i]) & 0xff for i in range(8))
        return out

    def fileno(self):
        return self.hidfp

    def get_status(self):
        with self.lock:
            result = (self.time, self.temp, self.co2)
        return result

    def set_temp(self, val):
        now = datetime.datetime.now()
        with self.lock:
            self.temp = val
            self.time = now
        if self.co2 >= 0:
            minute = now.minute
            reduced = int(minute / self.interval)
            if reduced != self.ivhelp:
                if self.numsamples > 0:
                    co2avg = self.co2sum / self.numsamples
                    tempavg = self.tempsum / self.numsamples
                    print "co2 %s, temp %s" % (co2avg, tempavg)
                    self.co2store.add(now, co2avg, tempavg)
                self.ivhelp = reduced
                self.numsamples = 0
                self.co2sum = 0
                self.tempsum = 0
            self.co2sum += self.co2
            self.tempsum += val
            self.numsamples += 1

    def set_co2(self, val):
        now = datetime.datetime.now()
        with self.lock:
            self.co2 = val
            self.time = now

    def open(self, hidraw):
        self.hidfp = open(hidraw, "a+b", 0)

        HIDIOCSFEATURE_9 = 0xC0094806
        set_report = "\x00" + "".join(chr(e) for e in self.key)
        fcntl.ioctl(self.hidfp, HIDIOCSFEATURE_9, set_report)

    def run(self):
        self.ops = {}

        while True:
            data = list(ord(e) for e in self.hidfp.read(8))
            decrypted = self.decrypt(data)
            if decrypted[4] != 0x0d or (sum(decrypted[:3]) & 0xff) != decrypted[3]:
                print data, " => ", decrypted,  "Checksum error"
            else:
                op = decrypted[0]
                val = decrypted[1] << 8 | decrypted[2]

                if op == 0x42:
                    self.set_temp(val / 16.0 - 273.15)
                elif op == 0x50:
                    self.set_co2(val)

                #self.ops[op] = True
                #if 0x50 in self.ops and 0x42 in self.ops:
                #    print "%s\t%2.2f\t%4i" % (self.time.isoformat(), self.temp, self.co2)
                #    self.ops = {}


class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    @classmethod
    def set_monitor(cls, co2):
        cls.co2 = co2

    log_file = open('/tmp/co2-server.log', 'w')
    def log_message(self, format, *args):
        self.log_file.write("%s - - [%s] %s\n" %
                            (self.client_address[0],
                             self.log_date_time_string(),
                             format%args))
        self.log_file.flush()

    def do_GET(self):
        resp = self.get_co2("GET")
        if len(resp) > 0:
            self.wfile.write(resp)
        else:
            pass #SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

    def do_HEAD(self):
        resp = self.get_co2("HEAD")
        if len(resp) == 0:
            pass #SimpleHTTPServer.SimpleHTTPRequestHandler.do_HEAD(self)

    def get_co2(self, reqtype):
        path = self.path.split('?',1)
        args = ""
        if len(path) > 1:
            args = path[1]
        path = path[0]
        path = path.split('#',1)[0]
        args = args.split('#',1)[0]
        if path == "/co2-status.txt":
            (timestamp, temp, co2) = MyRequestHandler.co2.get_status()
            color = "#009900"
            if co2 >= 800 and co2 < 1200:
                color = "#ff9900"
            elif co2 >= 1200:
                color = "#990000"
            text = "%s %2.1f\xc2\xb0\n%4i" % (color, temp, co2)
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Content-Length", str(len(text)))
            self.send_header("Last-Modified", timestamp.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            self.end_headers()
            return text
        elif path == "/co2-status.png":
            co2store = MyRequestHandler.co2.co2store
            (timestamp, temp, co2) = MyRequestHandler.co2.get_status()
            if reqtype == "GET":
                img = self.create_image(co2store, temp, co2)
            else:
                img = "SomeArbitraryButNonEmptyString"
            self.send_response(200)
            self.send_header("Content-type", "image/png")
            self.send_header("Content-Length", str(len(img)))
            self.send_header("Last-Modified", timestamp.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            self.end_headers()
            return img
        return ""

    def create_image(self, co2store, temp, co2):
        gp = subprocess.Popen(["gnuplot"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        command = """
set encoding utf8
set term pngcairo transparent enhanced nointerlace color linewidth 1 font "arial,12" size 400,400

set autoscale y
unset autoscale x
set xrange [0:(24*3600)]
set xtics ("" 6*3600, "" 12*3600, "" 18*3600) nomirror
set ytics 1 format "{/arial:Bold=22 %2.0f}" nomirror
set grid ytics lc rgb "#44ffffff" lw 1 lt 1
set margin 5, 2, 4, 1
set border 3 lc rgb "white" lw 5
set key tc rgb "white"
set label 1 at screen 0.5, screen 0.08 center back "{/arial:Bold $temp \302\260C | $co2 ppm}" font "arial,32" tc rgb "white"

plot '-' using 1:2 title '' linecolor '#888888' lw 5 with lines, '-' using 1:2 title '' linecolor '#ff4444' lw 5 with lines
"""
        for data in [co2store.get_range(co2store.yesterday(), True), co2store.get_range(co2store.today(), True)]:
            data_temp = [ ("%s %s" % (row[0], row[2])) for row in data ]
            command += "\n".join(data_temp) + "\ne\n"
        t = Template(command)
        command = t.substitute({'temp': ("%.1f" % temp).replace(".", ","),
                                'co2': str(co2) })
        #print "%s" % command
        (out, err) = gp.communicate(command)

        im = subprocess.Popen("convert - ( +clone -background black -shadow 80x5+3+3 -channel A -level 0,50% +channel ) -background none -compose DstOver -flatten -".split(), \
                              stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        (out, err) = im.communicate(out)
        return out


if __name__ == "__main__":
    co2 = Co2Monitor()
    co2.open(sys.argv[1])

    Handler = MyRequestHandler
    Handler.co2 = co2
    SocketServer.ThreadingTCPServer.allow_reuse_address = True
    server = SocketServer.ThreadingTCPServer(('0.0.0.0', 8080), Handler)

    thread = threading.Thread(target=server.serve_forever)
    thread.setName('WebServer')
    thread.setDaemon(True)
    thread.start()

    co2.run()

    sys.exit(0)
