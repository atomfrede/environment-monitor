#!/bin/env python2

import os.path
import csv
import time, datetime
import threading


class Co2Store:

  headers = ["time", "co2", "temp"]

  def __init__(self, fn):
    self.filename = fn
    # individual stored samples
    self.items = list()
    # timestamp of sample added most recently
    self.lasttime = datetime.datetime.fromtimestamp(0)
    # keep 2 days of sample in memory
    self.retain = datetime.timedelta(2)
    # create a lock for read/write access to self.items
    self.lock = threading.Lock()

    now = datetime.datetime.now()

    if os.path.exists(fn):
      limit = now - self.retain
      with open(fn, "r") as fh:
        line = fh.readline().strip()
        headers = line.split("\t")
        if headers != self.headers:
          raise Exception, "Invalid headers %s in file %s, expected %s" % (headers, fn, self.headers)
        for line in fh:
          line = line.strip()
          values = line.split("\t")
          valuedict = dict(zip(headers, values))
          tm = datetime.datetime.fromtimestamp(int(valuedict["time"]))
          self.lasttime = tm
          if tm >= limit:
            valuedict["time"] = tm
            self.items.append(valuedict)
      self.fh = open(fn, "a")
    else:
      self.fh = open(fn, "w")
      self.fh.write("\t".join(self.headers) + "\n")
      self.fh.flush

  def add(self, timestamp, co2, temp):
    item = dict(zip(self.headers, [timestamp, co2, temp]))
    itemarr = [str(int(time.mktime(timestamp.timetuple()))), str(co2), str(temp)]
    with self.lock:
      self.items.append(item)
      self.lasttime = timestamp
      self.fh.write("\t".join(itemarr) + "\n")
      self.fh.flush

  def get_last_modified(self):
    with self.lock:
      result = self.lasttime
    return result

  def get_range(self, startend, convert=False):
    base = int(time.mktime(startend[0].timetuple()))
    with self.lock:
      result = [
        [(int(time.mktime(x["time"].timetuple())) - base) if convert else x["time"], \
         int(x["co2"]) if convert else x["co2"],
         float(x["temp"]) if convert else x["temp"]] \
          for x in self.items if x["time"] >= startend[0] and x["time"] <= startend[1]]
    return result

  def today(self):
    now = datetime.datetime.now()
    start = now.replace(hour=0, minute=0, second=0, microsecond=0)
    end = start + datetime.timedelta(1)
    return (start, end)

  def yesterday(self):
    now = datetime.datetime.now()
    end = now.replace(hour=0, minute=0, second=0, microsecond=0)
    start = end - datetime.timedelta(1)
    return (start, end)

#####################################

if __name__ == "__main__":

  test = Co2Store("/tmp/test.csv")
  test.add(datetime.datetime.now(), 505, 23.2)
  time.sleep(2)
  test.add(datetime.datetime.now(), 495, 23.1)

  print("%s - %s" % test.today())
  print("%s - %s" % test.yesterday())

  print("get range today - %s" % test.get_range(test.today()))
  print("get range today - %s" % test.get_range(test.today(), True))

  from string import Template
  import subprocess

  gp = subprocess.Popen(["gnuplot"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
  command = """
set encoding utf8
set term pngcairo transparent enhanced nointerlace color linewidth 1 font "arial,12" size 400,400

set autoscale y
unset autoscale x
set xtics ("" 6*3600, "" 12*3600, "" 18*3600) nomirror
set ytics 1 format "{/arial:Bold=22 %2.0f}" nomirror
set grid ytics lc rgb "#44ffffff" lw 1 lt 1
set xrange [0:(24*3600)]
set margin 5, 2, 4, 1
set border 3 lc rgb "white" lw 5
set key tc rgb "white"
set label 1 at screen 0.5, screen 0.08 center back "{/arial:Bold $temp \302\260C | $co2 ppm}" font "arial,32" tc rgb "white"

plot '-' using 1:2 title '' linecolor '#888888' lw 5 with lines, '-' using 1:2 title '' linecolor '#ff4444' lw 5 with lines
"""
  for data in [test.get_range(test.yesterday(), True), test.get_range(test.today(), True)]:
    data_temp = [ ("%s %s" % (row[0], row[2])) for row in data ]
    command += "\n".join(data_temp) + "\ne\n"
  t = Template(command)
  command = t.substitute({'temp': "19,7",
                        'co2': "2524" })
  print "%s" % command
  (out, err) = gp.communicate(command)
  #with open("out.png", "wb") as img:
  #  img.write(out)

  im = subprocess.Popen("convert - ( +clone -background black -shadow 80x5+3+3 -channel A -level 0,50% +channel ) -background none -compose DstOver -flatten -".split(), \
                        stdout=subprocess.PIPE, stdin=subprocess.PIPE)
  (out, err) = im.communicate(out)

  with open("out.png", "wb") as img:
    img.write(out)
