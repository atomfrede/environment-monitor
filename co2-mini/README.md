# CO2 Mini Server Applications

This directory provides the following server applications for the TFA-Dostmann AirControl CO2-Mini device:

* c2m-console.py
* c2m-simple-server.py
* c2m-image-server.py

Along with these applications there are further files for

* UDEV rule
* systemd service file

## c2m-image-server.py

An HTML server generating a pretty temperature line chart along with the current temperature and CO<sub>2</sub> value.
It also provides a textual output of temperature and CO<sub>2</sub> value along with a CSS color code corresponding
to the LED which the CO2-Mini device displays.

The server listens on port 8080 (see source code) and can be accessed via:
    http://myserver:8080/co2-status.png
or
    http://myserver:8080/co2-status.txt

The generated image can be included in some web page or displayed directly on your smartphone using an app
like [URL Image Widget](https://play.google.com/store/apps/details?id=com.weite_welt.urlimagewidget).

Likewise the generated text can be used for further processing or displayed directly on your smartphone
using an app like
[Http Widget](https://play.google.com/store/apps/details?id=net.rosoftlab.httpwidget1).

**Example for co2-status.png:**

<img src="c2m-image-server-urlimagewidget.png" width="50%" height="50%">

**Example for co2-status.txt:**
```
#ff9900 19.5°
 842
```

<img src="c2m-image-server-httpwidget.png" width="50%" height="50%">

## c2m-simple-server.py

A simple HTML server for providing only the textual representation of temperature and CO<sub>2</sub> value as
described above for c2m-image-server.py.

The server listens on port 8080 (see source code) and can be accessed via:
    http://myserver:8080/co2-status.txt

## c2m-console.py

A script which just outputs the current temperature and CO<sub>2</sub> value as they become available from the device.
